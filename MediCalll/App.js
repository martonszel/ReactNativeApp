import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, Button, Alert } from 'react-native';

export default class App extends Component {
  constructor() {
    super()
    this.state = {
      value: ''
    };
    this.handleChangeText = this.handleChangeText.bind(this)
  }
  handleChangeText(newText) {
    this.setState({
      value: newText
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <Image source={require('./assets/MediCall.jpg')} style={{ width: 200, height: 200 }} />
        <Text style={{ margin:10}} >Üdv {this.state.value} !</Text>
        <View style={{flexDirection:"row" }}>
        <TextInput
          style={{ height: 40 , padding:5}}
          placeholder="Név"
          defaultValue={this.state.value}
          onChangeText={this.handleChangeText}
        />
        <Button 
          onPress={() => {
            Alert.alert('Üdv ' + this.state.value)
          }}
          title="Küldés"
        />
        </View> 
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
